<?php

namespace GetNoticed\CustomerLoginRedirect\Data;

interface RedirectOptionInterface
{

    public function getRedirectOptionCode(): string;

    public function getRedirectOptionLabel(): string;

    public function getRedirectOptionPath(): string;

    public function getRedirectOptionParams(): array;

}