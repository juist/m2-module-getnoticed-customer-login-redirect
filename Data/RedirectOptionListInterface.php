<?php

namespace GetNoticed\CustomerLoginRedirect\Data;

interface RedirectOptionListInterface
{

    public function getRedirectOptions(): array;

    public function getRedirectOptionByCode(string $code): RedirectOptionInterface;

}