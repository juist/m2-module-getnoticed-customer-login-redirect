<?php

namespace GetNoticed\CustomerLoginRedirect\Data;

use GetNoticed\CustomerLoginRedirect as CLR;

class RedirectOptionList
    implements RedirectOptionListInterface
{

    /**
     * @var \GetNoticed\CustomerLoginRedirect\Data\RedirectOptionInterface[]
     */
    protected $options = [];

    public function __construct(
        array $options = []
    ) {
        $this->options = $options;
    }


    public function getRedirectOptions(): array
    {
        return $this->options;
    }

    /**
     * @throws \GetNoticed\CustomerLoginRedirect\Exception\RedirectOptionNotFoundException
     */
    public function getRedirectOptionByCode(string $code): RedirectOptionInterface
    {
        foreach ($this->options as $option) {
            if ($option->getRedirectOptionCode() === $code) {
                return $option;
            }
        }

        throw new CLR\Exception\RedirectOptionNotFoundException(__('Redirect option with code `%1` not found', $code));
    }

}