# Magento 2 - GetNoticed CustomerLoginRedirect

# File is automatically generated, please change TEMPLATE.md!

## Unit Tests

## Setup Unit Tests

1. Rename $MAGENTO_ROOT/dev/tests/unit/phpunit.xml.dist to phpunit.xml
2. Add test in phpunit.xml

```
<testsuite name="CustomerLoginRedirect">
    <directory suffix="Test.php">../../../app/code/GetNoticed/CustomerLoginRedirect/Test/Unit</directory>
    <directory suffix="Test.php">../../../vendor/GetNoticed/CustomerLoginRedirect/Test/Unit</directory>
</testsuite>
```

### Running Unit Tests

Execute bash on running container.

```
cd $MAGENTO_ROOT/dev/tests/unit/
../../../vendor/bin/phpunit
```

### View Unit Tests

Execute bash on running container.

```
cd $MAGENTO_ROOT/dev/tests/unit/
../../../vendor/bin/phpunit --suite CustomerLoginRedirect --testdox
```

## Integration Tests

### Setup Integration Tests

1. Rename $MAGENTO_ROOT/dev/tests/integration/phpunit.xml.dist to phpunit.xml
2. Add test in phpunit.xml

```
<testsuite name="CustomerLoginRedirect">
    <directory suffix="Test.php">../../../app/code/GetNoticed/CustomerLoginRedirect/Test/Integration</directory>
    <directory suffix="Test.php">../../../vendor/GetNoticed/CustomerLoginRedirect/Test/Integration</directory>
</testsuite>
```

### Running Integration Tests

Execute bash on running container.

```
cd $MAGENTO_ROOT/dev/tests/integration/
../../../vendor/bin/phpunit --suite CustomerLoginRedirect
```

### View Integration Tests

Execute bash on running container.

```
cd $MAGENTO_ROOT/dev/tests/integration/
../../../vendor/bin/phpunit --testdox
```

# Test Results

****
## Test FAILED - Wed Dec 27, 2017 11:58
****

### Tests

#### Unit Tests

```
Error generating test dox: 

Fatal error: Class 'PHPUnit\Framework\BaseTestListener' not found in /home/wvestjens/magento/workspace/4phones/vendor/magento/framework/TestFramework/Unit/Listener/ReplaceObjectManager.php on line 14

Call Stack:
    0.0001     363640   1. {main}() /home/wvestjens/magento/workspace/4phones/vendor/phpunit/phpunit/phpunit:0
    0.0093    1345064   2. PHPUnit_TextUI_Command::main() /home/wvestjens/magento/workspace/4phones/vendor/phpunit/phpunit/phpunit:55
    0.0093    1345160   3. PHPUnit_TextUI_Command->run() /home/wvestjens/magento/workspace/4phones/vendor/phpunit/phpunit/src/TextUI/Command.php:132
    0.0179    2370600   4. PHPUnit_TextUI_TestRunner->doRun() /home/wvestjens/magento/workspace/4phones/vendor/phpunit/phpunit/src/TextUI/Command.php:179
    0.0179    2370624   5. PHPUnit_TextUI_TestRunner->handleConfiguration() /home/wvestjens/magento/workspace/4phones/vendor/phpunit/phpunit/src/TextUI/TestRunner.php:188
    0.0180    2372656   6. class_exists() /home/wvestjens/magento/workspace/4phones/vendor/phpunit/phpunit/src/TextUI/TestRunner.php:743
    0.0180    2372752   7. spl_autoload_call() /home/wvestjens/magento/workspace/4phones/vendor/phpunit/phpunit/src/TextUI/TestRunner.php:743
    0.0180    2372848   8. Composer\Autoload\ClassLoader->loadClass() /home/wvestjens/magento/workspace/4phones/vendor/phpunit/phpunit/src/TextUI/TestRunner.php:743
    0.0180    2373008   9. Composer\Autoload\includeFile() /home/wvestjens/magento/workspace/4phones/vendor/composer/ClassLoader.php:322
    0.0181    2380040  10. include('/home/wvestjens/magento/workspace/4phones/vendor/magento/framework/TestFramework/Unit/Listener/ReplaceObjectManager.php') /home/wvestjens/magento/workspace/4phones/vendor/composer/ClassLoader.php:444

```
#### Integration Tests

```
Error generating test dox: 

=== Memory Usage System Stats ===
Memory usage (OS):	31.77M (794.14% of 4.00M reported by PHP)
Estimated memory leak:	27.77M (87.41% of used memory)

```

### Test results

#### Unit test results

```

Fatal error: Class 'PHPUnit\Framework\BaseTestListener' not found in /home/wvestjens/magento/workspace/4phones/vendor/magento/framework/TestFramework/Unit/Listener/ReplaceObjectManager.php on line 14

Call Stack:
    0.0001     363600   1. {main}() /home/wvestjens/magento/workspace/4phones/vendor/phpunit/phpunit/phpunit:0
    0.0098    1345024   2. PHPUnit_TextUI_Command::main() /home/wvestjens/magento/workspace/4phones/vendor/phpunit/phpunit/phpunit:55
    0.0098    1345120   3. PHPUnit_TextUI_Command->run() /home/wvestjens/magento/workspace/4phones/vendor/phpunit/phpunit/src/TextUI/Command.php:132
    0.0181    2231712   4. PHPUnit_TextUI_TestRunner->doRun() /home/wvestjens/magento/workspace/4phones/vendor/phpunit/phpunit/src/TextUI/Command.php:179
    0.0181    2231736   5. PHPUnit_TextUI_TestRunner->handleConfiguration() /home/wvestjens/magento/workspace/4phones/vendor/phpunit/phpunit/src/TextUI/TestRunner.php:188
    0.0182    2233768   6. class_exists() /home/wvestjens/magento/workspace/4phones/vendor/phpunit/phpunit/src/TextUI/TestRunner.php:743
    0.0182    2233864   7. spl_autoload_call() /home/wvestjens/magento/workspace/4phones/vendor/phpunit/phpunit/src/TextUI/TestRunner.php:743
    0.0182    2233960   8. Composer\Autoload\ClassLoader->loadClass() /home/wvestjens/magento/workspace/4phones/vendor/phpunit/phpunit/src/TextUI/TestRunner.php:743
    0.0182    2234120   9. Composer\Autoload\includeFile() /home/wvestjens/magento/workspace/4phones/vendor/composer/ClassLoader.php:322
    0.0183    2241192  10. include('/home/wvestjens/magento/workspace/4phones/vendor/magento/framework/TestFramework/Unit/Listener/ReplaceObjectManager.php') /home/wvestjens/magento/workspace/4phones/vendor/composer/ClassLoader.php:444

```

#### Integration test results

```

=== Memory Usage System Stats ===
Memory usage (OS):	31.57M (789.36% of 4.00M reported by PHP)
Estimated memory leak:	27.57M (87.33% of used memory)

```