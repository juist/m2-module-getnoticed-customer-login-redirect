<?php

namespace GetNoticed\CustomerLoginRedirect\Config\Source;

use GetNoticed\CustomerLoginRedirect;
use Magento\Framework;

class RedirectOptionSource
    implements Framework\Option\ArrayInterface
{

    /**
     * @var CustomerLoginRedirect\Data\RedirectOptionListInterface
     */
    protected $redirectOptionList;

    public function __construct(
        CustomerLoginRedirect\Data\RedirectOptionListInterface $redirectOptionList
    ) {
        $this->redirectOptionList = $redirectOptionList;
    }

    public function toOptionArray()
    {
        $options = [];

        foreach ($this->redirectOptionList->getRedirectOptions() as $redirectOption) {
            /** @var \GetNoticed\CustomerLoginRedirect\Data\RedirectOptionInterface $redirectOption */
            $options[] = [
                'value' => $redirectOption->getRedirectOptionCode(),
                'label' => $redirectOption->getRedirectOptionLabel()
            ];
        }

        return $options;
    }

}