<?php

namespace GetNoticed\CustomerLoginRedirect\Helper\Config;

use GetNoticed\CustomerLoginRedirect as CLR;

interface GeneralInterface
{

    public function isModuleEnabled(): bool;

    public function getRedirectOption(): CLR\Data\RedirectOptionInterface;

}