<?php

namespace GetNoticed\CustomerLoginRedirect\Helper\Config;

use GetNoticed\Common;
use GetNoticed\CustomerLoginRedirect as CLR;
use Magento\Store;

class General
    extends Common\Helper\Config\AbstractConfigHelper
    implements GeneralInterface
{

    const XML_PATH_BASE = 'getnoticed_customerloginredirect/general';
    const XML_PATH_ENABLED = '%s/enabled';
    const XML_PATH_REDIRECT_OPTION = '%s/redirect_option';

    /**
     * @var CLR\Data\RedirectOptionListInterface
     */
    protected $redirectOptionList;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        CLR\Data\RedirectOptionListInterface $redirectOptionList
    ) {
        $this->redirectOptionList = $redirectOptionList;

        parent::__construct($context, $storeManager);
    }

    public function isModuleEnabled(): bool
    {
        return $this->scopeConfig->isSetFlag(
            sprintf(self::XML_PATH_ENABLED, self::XML_PATH_BASE),
            Store\Model\ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()
        );
    }

    /**
     * @throws \GetNoticed\CustomerLoginRedirect\Exception\RedirectOptionNotFoundException
     */
    public function getRedirectOption(): CLR\Data\RedirectOptionInterface
    {
        $redirectOptionCode = $this->scopeConfig->getValue(
            sprintf(self::XML_PATH_REDIRECT_OPTION, self::XML_PATH_BASE),
            Store\Model\ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()
        );

        if ($redirectOptionCode === null || strlen($redirectOptionCode) < 1) {
            throw new CLR\Exception\RedirectOptionNotFoundException(__('Code is empty'));
        }

        return $this->redirectOptionList->getRedirectOptionByCode($redirectOptionCode);
    }

}