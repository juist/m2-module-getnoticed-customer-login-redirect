<?php

namespace GetNoticed\CustomerLoginRedirect\Exception;

use Magento\Framework\Exception;

abstract class AbstractException
    extends Exception\LocalizedException
{

}