<?php

namespace GetNoticed\CustomerLoginRedirect\Exception;

class RedirectOptionNotFoundException
    extends AbstractException
{
}