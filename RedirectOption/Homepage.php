<?php

namespace GetNoticed\CustomerLoginRedirect\RedirectOption;

use Magento\Framework;
use GetNoticed\CustomerLoginRedirect as CLR;

class Homepage
    implements CLR\Data\RedirectOptionInterface
{

    /**
     * @var \Magento\Framework\Url
     */
    protected $urlBuilder;

    public function __construct(
        Framework\Url $urlBuilder
    ) {
        $this->urlBuilder = $urlBuilder;
    }

    const REDIRECT_OPTION_CODE = 'homepage';

    public function getRedirectOptionCode(): string
    {
        return self::REDIRECT_OPTION_CODE;
    }

    public function getRedirectOptionLabel(): string
    {
        return __('Redirect to homepage');
    }

    public function getRedirectOptionPath(): string
    {
        return '';
    }

    public function getRedirectOptionParams(): array
    {
        return [];
    }

}