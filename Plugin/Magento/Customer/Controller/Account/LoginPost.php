<?php

namespace GetNoticed\CustomerLoginRedirect\Plugin\Magento\Customer\Controller\Account;

use GetNoticed\CustomerLoginRedirect as CLR;

/**
 * Class LoginPost
 * @package GetNoticed\CustomerLoginRedirect\Plugin\Magento\Customer\Controller\Account
 */
class LoginPost
{

    /**
     * @var CLR\Helper\Config\GeneralInterface
     */
    protected $generalConfig;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * LoginPost constructor.
     * @param CLR\Helper\Config\GeneralInterface $generalConfig
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     */
    public function __construct(
        CLR\Helper\Config\GeneralInterface $generalConfig,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
    ) {
        $this->generalConfig = $generalConfig;
        $this->customerSession = $customerSession;
        $this->customerRepository = $customerRepository;
    }


    /**
     * @param \Magento\Customer\Controller\Account\LoginPost $loginPostController
     * @param $response
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function afterExecute(
        \Magento\Customer\Controller\Account\LoginPost $loginPostController,
        $response
    ) {

        //retrieve customer
        try{
            $customerId = $this->customerSession->getData('customer_id');
            $customer = $this->customerRepository->getById($customerId);
            $isApproved = $customer->getCustomAttribute('eh_is_approved')->getValue();
        }catch (\Exception $exception){
            //don't throw exception just return original response;
            return $response;
        }

        // Check if enabled
        if ($this->generalConfig->isModuleEnabled() !== true) {
            return $response;
        }

        // Get redirect option (or fail if not set)
        try {
            $redirectOption = $this->generalConfig->getRedirectOption();
        } catch (CLR\Exception\RedirectOptionNotFoundException $e) {
            return $response;
        }

        // Check response
        /** @var \Magento\Framework\Controller\Result\Redirect $response */
        if (!$response instanceof \Magento\Framework\Controller\Result\Redirect) {
            return $response;
        }

        // if not approved keep original response, this contains url to "not approved" cms page"
        if(!(bool)$isApproved){
            return $response;
        }

        //otherwise we set redirect to homepage
        $response->setPath(
            $redirectOption->getRedirectOptionPath(),
            $redirectOption->getRedirectOptionParams()
        );

        return $response;
    }

}